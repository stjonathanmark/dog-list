import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DogService } from '../services/dog.service';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html'
})
export class BrowseComponent implements OnInit {
    
    loading: boolean = true;
    breed: string = '';
    imageUrl: string = '';
    errorMsg: string = '';

    constructor(private route: ActivatedRoute, private dogService: DogService) { }

    ngOnInit(): void {
        this.route.paramMap.subscribe(params => {
            this.breed = params.get('breed'); 
            this.getRandomImage();
        });
    }

    async getRandomImage() {
        this.loading = true;
        this.imageUrl = '';
        this.errorMsg = '';

        try {
            const response: any = await this.dogService.getRandomImage(this.breed.replace('-', '/'));
            if (response.status === 'success') 
                this.imageUrl = response.message;
            else 
                this.errorMsg = "An unknown error occurred retrieving image";

            this.loading = false;
        } catch (exception) {
            this.errorMsg = exception.error.message;
            this.loading = false;
        }
    }

}
