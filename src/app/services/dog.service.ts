import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DogService {

    private key: string = 'breeds';

    constructor(private http: HttpClient) { }

    async getAllBreeds(): Promise<any> {
        return await this.http.get("https://dog.ceo/api/breeds/list/all").toPromise();
    }

    async loadBreeds(): Promise<void> {
        if (this.breedList.length) return;

        const response = await this.getAllBreeds();
        const breeds = response.message;

        let arrBreeds = [];

        for (let breed in breeds) {

            let objBreed = { name: breed };

            arrBreeds.push(objBreed);
            
            const subBreeds = breeds[breed];

            if (subBreeds.length) {
                subBreeds.forEach(subBreed => {
                    arrBreeds.push({ 
                        name: `${breed}-${subBreed}`
                    });
                });
            }
        }

        this.saveAllBreeds(arrBreeds);
    }

    async getRandomImage(breed: string): Promise<any> {
        return await this.http.get(`https://dog.ceo/api/breed/${breed}/images/random`).toPromise();
    }

    saveAllBreeds(breeds: Array<any>) {
        sessionStorage.setItem(this.key, JSON.stringify(breeds));
    }

    get breedList(): Array<any> {
        return (<Array<any>>JSON.parse(sessionStorage.getItem(this.key) || '[]'));
    }

}
