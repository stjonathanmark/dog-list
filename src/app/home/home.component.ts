import { Component, OnInit } from '@angular/core';
import { DogService } from '../services/dog.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    loading: boolean = true;
    breeds: Array<any> = [];
    errorMsg: string = '';

    constructor(private dogService: DogService) { }

    async ngOnInit() {
        this.loading = true;
        this.errorMsg = '';

        try {
            await this.dogService.loadBreeds();
            this.breeds = this.dogService.breedList;   
            this.loading = false;
        } catch {
            this.errorMsg = 'An unknown error occurred while retrieving breed list. Please try Again.';
            this.loading = false;
        }
    }
}
