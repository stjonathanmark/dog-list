import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: '<h2>Requested Resource Not Found</h2>'
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
