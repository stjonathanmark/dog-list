# Dog List
This is demo project made with Angular 10 that displays dog breads and browses dog images for a chosen breed. 

## Instructions 
1. To run this application you will need to download and install **NodeJs**. [Download NodeJs](https://nodejs.org/en/)
2. Open the **'NodeJs Command Line Tool'** and set it's path to the **'dog-list'** folder (a. k. a. Application Root) where ever you placed source code on your computer after clone/download.
    * **NOTE**: You can you use any command line tool of your choosing as long as it has authorization and the location the **NodeJs** program files within it's path/view.
    * The **'NodeJs Command Line Tool'** is mentioned in these instructions, because it is installed with **NodeJs** by default and requires no further configuration to run node and npm commands.
3. Type and run the command **'ng serve'** in the NodeJs command line tool to start the angular application
4. Open any browser and type [**'http://localhost:4200'**](http://localhost:4200) in the address bar and press enter 
5. Enjoy!!!!!!!  